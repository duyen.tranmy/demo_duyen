 <!--  PRODUCT DETAIL -->
 <?php 
  
    include_once('./system/product_detail.php');
   

 ?>
 <?php 
    if(isset($_POST['txtNoteQA']) && $_POST['txtNoteQA'] != '' ) {
      include_once('./system/config.php');
      if(!isset($_SESSION)) 
      { 
          session_start(); 
      } 
      $txtNoteQA = $_POST['txtNoteQA'];
  
      if(!isset($_SESSION['login'])) {
          echo "<script>alert('Vui lòng đăng nhập tài khoản');</script>";
          echo "<script type='text/javascript'> document.location = '../index.php'; </script>";
  
      }
      
      $id = $_GET['id'];
      $idUser = $_SESSION['login'];
      $fullname1 = $_SESSION['fullname'];
      $sql ="INSERT INTO `recommend`( `question`, `auth`, `product`,  `name_user`) VALUES (:qa,:idUser,:id,:fname)" ;
      $query= $dbh -> prepare($sql);
      $query-> bindParam(':qa', $txtNoteQA, PDO::PARAM_STR);
      $query-> bindParam(':idUser', $idUser, PDO::PARAM_STR);
      $query-> bindParam(':id', $id, PDO::PARAM_STR);
      $query-> bindParam(':fname', $fullname1, PDO::PARAM_STR);
      $query-> execute();
      $lastInsertId = $dbh->lastInsertId();
          
      if($lastInsertId)
      {
          echo "<script>alert('Send thành công');</script>";
          unset($_POST['txtNoteQA']);
      }
      else 
      {
          echo "<script>alert('send thất bại vui lòng thử lại sau');</script>";
      }
    }

    
?>
<?php 
 include_once('./system/send.php');
?>
 <div class="product-detail-wrap mt-4">
    <div class="container">
      <div class="row product-detail">
        <h4 class="title block-mobile"><?=$products_detail[0]->title?></h4>
        <div class="col-12 col-md-6 product-images">
          <div class="row">
            <div class="col-3 product-image-list custom-scroll pr-1">
              <div class="product-image-item">
                <img src="<?php echo './img/'.$products_detail[0]->image ?>" data-image="<?php echo './img/'.$products_detail[0]->image ?>">
              </div>
              <div class="product-image-item active">
                <img src="<?php echo './img/'.$products_detail[0]->image ?>" data-image="<?php echo './img/'.$products_detail[0]->image ?>">
              </div>
            </div>
            <div class="col-9 product-image-featured">
              <img id="product-image-featured" src="<?php echo './img/'.$products_detail[0]->image ?>"
                   data-zoom-image="<?php echo './img/'.$products_detail[0]->image ?>"/>
            </div>
          </div>
          <div class="row pro-share product-share">
            <div class="col-12 col-lg-5 item-col-product">
              <div class="share-social d-flex">
                <span class="">Chia sẻ tới:</span>
                <a class="ml-3 social-button" href="#"><i
                  class="fab fa-facebook-square color-blue-dark font-20"></i></a>
                <a class="ml-2 social-button" href="#"><i class="fab fa-twitter color-blue-dark font-20"></i></a>
                <a class="ml-2 social-button zb-btn-blue--20x20" href="#"><i class="zb-logo-zalo"></i></a>
              </div>
            </div>
            <div class="col-12 col-lg-3 item-col-product" style="border-left:1px solid #e4e4e4">
              <span class="like-product">
                <svg width="24" height="20" class="svg-like">
                    <path
                      d="M19.469 1.262c-5.284-1.53-7.47 4.142-7.47 4.142S9.815-.269 4.532 1.262C-1.937 3.138.44 13.832 12 19.333c11.559-5.501 13.938-16.195 7.469-18.07z"
                      stroke="#FF424F" stroke-width="1.5" fill="none" fill-rule="evenodd"
                      stroke-linejoin="round" style="fill: none;"></path>
                </svg>
              </span>
              <span>Đã thích</span><span class="totalLike">(6) </span>
            </div>
            <div class="col-12 col-lg-4 item-col-product" style="border-left:1px solid #e4e4e4">
              <button class="button-report-product"><i
                class="fa fa-flag-checkered"></i>&nbsp;Phản ánh vi phạm
              </button>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6 product-detail-info color-gray-dark">
          <div class="scroll-detail">
            <h1 class="title none-mobile"><?=$products_detail[0]->title?></h1>
            <div class="d-flex align-items-center">
              <div class="star-list mr-2">
                <span class="star-item active">
                  <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                  <img src="./img/icon-star.png" width="12px" alt="star">
                </span>
                <span class="star-item active">
                  <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                  <img src="./img/icon-star.png" width="12px" alt="star">
                </span>
                <span class="star-item active">
                  <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                  <img src="./img/icon-star.png" width="12px" alt="star">
                </span>
                <span class="star-item">
                  <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                  <img src="./img/icon-star.png" width="12px" alt="star">
                </span>
                <span class="star-item">
                  <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                  <img src="./img/icon-star.png" width="12px" alt="star">
                </span>
              </div>
              <a class="pdp-link">6 đánh giá</a>
              <div class="pdp-review-summary__divider"></div>
              <a class="pdp-link">4 câu hỏi</a>
              <div class="pdp-review-summary__divider"></div>
              <span class="pdp-link">75 lượt xem</span>
            </div>
            <div class="flex v-center">
              <span class="price-old"> </span>
              <span class="price-detail">&nbsp;<?=$products_detail[0]->price?> ₫</span>
            </div>
            <div class="group-size-sx">
              <div class="row-sx nsx clearfix">
                <label class="label-sx">Nhà cung cấp:</label>
                <a class="color-green-light" style="font-size: 17px" href="#">
                <?=$products_detail[0]->nameBrand?>
                </a>
              </div>
              <div class="row-sx vsx clearfix">
                <label class="label-sx">Địa chỉ:</label>
                <?=$products_detail[0]->address?>
              </div>
              <div class="row-sx pp clearfix">
                <label class="label-sx">Trạng thái:</label>
                <span style="">Còn hàng</span>
              </div>
              <div class="row-sx pp clearfix">
                <label class="label-sx">Đơn vị tính:</label>
                <span style="">Kg</span>
              </div>
            </div>
            <div class="group-size size">
              <div class="">
                <label class="label-sx">Số lượng:</label>
                <span>
                  <input type="text" name="quantity" value="1" size="2" id="qty" class="input-text qty" maxlength="12"
                         iswarehouse="0" maxvalue="0" orderoveravailable="0">
                  <button class="reduced items-count" onclick="reduced()" type="button"><i
                    class="icon-minus"> - </i></button>
                  <button class="increase items-count" onclick="increase()" type="button"><span> + </span></button>
                </span>
              </div>
            </div>
            <div class="row button-array">
              <div class="col-12 col-sm-6 padding-right-5 add">
              <form action="system/card.php" method="POST">
                <button type="submit" class="btn-add-cart buy-now btn">
                  <img class="mb-1 mr-2" src="./img/icon-cart-white.png" width="16px"> Cho vào giỏ
                </button>
              </form>
                
              </div>
              <div class="col-12 col-sm-6 padding-right-5 action">
              <a href="?a=order" type="button" class="btn-add-order buy-now btn">Đặt hàng</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row product-page-seller-info v-center">
        <div class="product-page-seller-info__section-1">
          <div class="product-page-seller-info__header-wrapper">
            <a class="product-page-seller-info__header-portrait">
              <div class="avatar-tp">
                <img class="avatar__img" src="./img/avatar-seller.jpeg">
              </div>
            </a>
            <div class="product-page-seller-info__header-info">
              <a class="product-page-seller-info__name-status" href="#">
                <div class="product-page-seller-info__shop-name"><?=$products_detail[0]->nameBrand?></div>
              </a>
              <div class="product-page-seller-info__buttons">
                <a class="product-page-seller-info__view-shop" href="#">
                  <button class="btn btn-light btn--s btn--inline d-flex align-items-center">Thăm gian hàng</button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="product-page-seller-info__section-2">
          <div class="product-page-seller-info__item ">
            <div class="product-page-seller-info__item__wrapper">
              <div class="product-page-seller-info__item__header">
                <span class="product-page-seller-info__item__primary-text">4</span>
              </div>
              <div class="product-page-seller-info__item__complement-text">Sản phẩm</div>
            </div>
            <div class="product-page-seller-info__item__separator"></div>
          </div>
          <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
              <div class="product-page-seller-info__item__header">
                <span class="product-page-seller-info__item__primary-text">8</span>
              </div>
              <div class="product-page-seller-info__item__complement-text">Đánh giá</div>
            </div>
            <div class="product-page-seller-info__item__separator"></div>
          </div>
          <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
              <div class="product-page-seller-info__item__header">
                <span class="product-page-seller-info__item__primary-text">6</span>
              </div>
              <div class="product-page-seller-info__item__complement-text">Lượt hỏi đáp</div>
            </div>
            <div class="product-page-seller-info__item__separator"></div>
          </div>

          <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
              <div class="product-page-seller-info__item__header">
                <span class="product-page-seller-info__item__primary-text">04/12/2018</span>
              </div>
              <div class="product-page-seller-info__item__complement-text">Ngày tham gia</div>
            </div>
            <div class="product-page-seller-info__item__separator"></div>
          </div>
        </div>
      </div>
      <div class="row product-tabs-info color-gray-dark mt-3">
        <div class="col-12 content-product-detail">
          <ul class="nav nav-tabs clearfix">
            <li class="col-12 p-0 col-sm-auto">
              <a class="active" href="#tab0" data-toggle="tab" aria-expanded="false">Giới thiệu</a>
            </li>
            <li class="col-12 p-0 col-sm-auto"><a href="#tab1" data-toggle="tab" aria-expanded="false">Thông tin chi
              tiết</a></li>
            <li class="col-12 p-0 col-sm-auto"><a href="#tab2" data-toggle="tab" aria-expanded="true">Đánh giá sản
              phẩm</a>
            </li>
            <li class="col-12 p-0 col-sm-auto"><a href="#tab3" data-toggle="tab" aria-expanded="false">Hỏi &amp; Đáp</a>
            </li>
            <li class="col-12 p-0 col-sm-auto"><a href="#tab4" data-toggle="tab" aria-expanded="false">Địa điểm</a></li>
          </ul>
          <div class="wrap-content-product">
            <div class="tab-content">
              <div class="tab-pane active" id="tab0">
                <div class="mod-title">
                  <h2 class="pdp-mod-section-title outer-title">Giới thiệu <?=$products_detail[0]->content?></h2>
                </div>
                <div class="pdp-mod-detail" style="padding: 0 15px">

                </div>
              </div>
              <div class="tab-pane" id="tab1">
                <div class="content pdp-mod-review">
                  <div class="product-info-detail">
                    <div class="row">
                      <div class="col-md-6 col-xs-12">
                        <h4 class="col-md-12 col-xs-12">Thông tin sản phẩm</h4>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Tên sản phẩm</div>
                          <div class="col-md-7 col-xs-7"><?=$products_detail[0]->title?></div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Cách bảo quản</div>
                          <div class="col-md-7 col-xs-7"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Cách bảo quản</div>
                          <div class="col-md-7 col-xs-7"></div>
                        </div>
                      </div>
                      <div class="col-md-6 col-xs-12">
                        <h4 class="col-md-12 col-xs-12">Thông tin Cơ sở sản xuất</h4>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Tên cơ sở</div>
                          <div class="col-md-7 col-xs-7">
                          <?=$products_detail[0]->nameBrand?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Số giấy phép đăng ký</div>
                          <div class="col-md-7 col-xs-7">
                            Số 123456789, ngày cấp 31/03/1980, đơn vị cấp UBND huyện Thọ Xuân
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-xs-5">Quản lý về VSATTP bởi</div>
                          <div class="col-md-7 col-xs-7">
                            UBND Huyện Thọ Xuân
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab2">
                <div class="content pdp-mod-review">
                  <div class=" product-rating-overview">
                    <div class="mod-title">
                      <h2 class="pdp-mod-section-title outer-title">Đánh giá và nhận xét của: <?=$products_detail[0]->title?></h2>
                    </div>
                    <div class="mod-rating">
                      <div class="summary">
                        <div class="score"><span>4,5</span> trên 5</div>
                        <div class="average">
                          <div class="container-star " style="width:166.25px;height:30px">
                            <div class="star-list mr-2">
                              <span class="star-item active">
                                <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                                <img src="./img/icon-star.png" width="12px" alt="star">
                              </span>
                              <span class="star-item active">
                                <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                                <img src="./img/icon-star.png" width="12px" alt="star">
                              </span>
                              <span class="star-item active">
                                <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                                <img src="./img/icon-star.png" width="12px" alt="star">
                              </span>
                              <span class="star-item">
                                <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                                <img src="./img/icon-star.png" width="12px" alt="star">
                              </span>
                              <span class="star-item">
                                <img src="./img/icon-star-empty.png" width="12px" alt="star empty">
                                <img src="./img/icon-star.png" width="12px" alt="star">
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="count">6 đánh giá</div>
                      </div>
                      <div class="detail">
                        <ul>
                          <li>
                            <span><span class="star-number">5</span> SAO</span>
                            <span class="progress-wrap">
                              <div class="pdp-review-progress">
                                <div class="bar bg"></div>
                                <div class="bar fg" style="width:50%"></div>
                              </div>
                            </span>
                            <span class="percent">3</span>
                          </li>
                          <li>
                            <span><span class="star-number">4</span> SAO</span>
                            <span class="progress-wrap">
                              <div class="pdp-review-progress">
                                <div class="bar bg"></div>
                                <div class="bar fg" style="width:50%"></div>
                              </div>
                            </span>
                            <span class="percent">3</span>
                          </li>
                          <li>
                            <span><span class="star-number">3</span> SAO</span>
                            <span class="progress-wrap">
                              <div class="pdp-review-progress">
                                <div class="bar bg"></div>
                                <div class="bar fg" style="width:50%"></div>
                              </div>
                            </span>
                            <span class="percent">3</span>
                          </li>
                          <li>
                            <span><span class="star-number">2</span> SAO</span>
                            <span class="progress-wrap">
                              <div class="pdp-review-progress">
                                <div class="bar bg"></div>
                                <div class="bar fg" style="width:50%"></div>
                              </div>
                            </span>
                            <span class="percent">3</span>
                          </li>
                          <li>
                            <span><span class="star-number">1</span> SAO</span>
                            <span class="progress-wrap">
                              <div class="pdp-review-progress">
                                <div class="bar bg"></div>
                                <div class="bar fg" style="width:0%"></div>
                              </div>
                            </span>
                            <span class="percent">0</span>
                          </li>
                        </ul>
                      </div>
                      <div class="">
                        <button type="button" class="btn next-btn next-btn-primary next-btn-medium btn-rate" onclick="showModalRate()">
                          ĐÁNH GIÁ SẢN PHẨM NÀY
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="mod-reviews">
                    <div class="item">
                      <div class="top">
                        <div class="star-list">
                    <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                        </div>
                        <span class="right">10/12/2018</span>
                      </div>
                      <div class="middle">
                                    <span>bởi<!-- --> <!-- -->
                                        <a href="#">Công ty cổ phần thực phẩm xanh HC</a>
                                    </span>
                      </div>
                      <div class="content">Dưa rất ngọt, ngon</div>
                      <div class="bottom">
                      </div>
                    </div>
                    <div class="item">
                      <div class="top">
                        <div class="star-list">
                    <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                        </div>
                        <span class="right">10/12/2018</span>
                      </div>
                      <div class="middle">
                                    <span>bởi<!-- --> <!-- -->
                                        <a href="#">PicFood.vn</a>
                                    </span>
                      </div>
                      <div class="content">Dưa ngon, tên đẹp nữa !</div>
                      <div class="bottom">
                      </div>
                    </div>
                    <div class="item">
                      <div class="top">
                        <div class="star-list">
                    <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                          <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                    </span>
                        </div>
                        <span class="right">10/12/2018</span>
                      </div>
                      <div class="middle">
                                    <span>bởi<!-- --> <!-- -->
                                        <a href="#">AT Food</a>
                                    </span>
                      </div>
                      <div class="content">Cũng khá ngon</div>
                      <div class="bottom">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab3">
                <div class="content pdp-mod-review">
                  <div class=" product-rating-overview">
                    <div class="mod-title">
                      <h2 class="pdp-mod-section-title outer-title">Hỏi và Đáp của: Dưa vàng Hoàng Kim Hậu</h2>
                    </div>
                    <div class="fs-dtrtcmtbox">
                    <form method="POST">
                        <textarea id="txtNoteQA" name="txtNoteQA" rows="3" class="f-cmttarea fsformsc"
                                  placeholder="Vui lòng nhập câu hỏi của bạn!"></textarea>
                      <div class="fs-dtrtbots clearfix">
                      </div>
                      <div class="qa-btn-footer">
                        
                        <input type='hidden' name='id' value='<?php echo $products_detail[0]->id;?>' />
                        <button type="submit" name="asa" class="btn btn-primary btn-delete">Gửi câu
                          hỏi
                        </button>
                        
                      </div>
                    </form>

                    </div>
                    <div class="mod-reviews">
                      <?php 
                        for($i = 0 ;$i < count($recom); $i++) {
                          ?>
                        <div class="item">
                        <div class="">
                          <img width="60" height="60" src="./img/product-8.jpeg">
                          <strong class="f-cmname"><!-- --> <!-- -->
                            <a href="#"><?php echo $recom[$i]->name_user ?></a>
                          </strong>
                          <span class="f-cmtime"><?php echo $recom[$i]->created ?></span>
                        </div>
                        <div class="content f-cmmain"><?php echo $recom[$i]->question ?></div>
                        <p class="f-cmbott"><span class="answer-comment">Trả lời</span>
                        </p>
                        <div id="commentAnswer146" class="fs-dtrtcmtbox commentAnswer" style="display: none;">
                            <textarea id="txtNoteAnswer146" name="txtNoteAnswer146" rows="3" class="f-cmttarea fsformsc"
                                      placeholder="Vui lòng nhập câu trả lời của bạn!"></textarea>
                          <div class="fs-dtrtbots clearfix">
                          </div>
                          <div class="qa-btn-footer">
                            <button type="button" class="btn btn-primary btn-delete">
                              Gửi câu trả lời
                            </button>
                          </div>
                        </div>
                      </div>
                          <?php
                        }
                      ?>
                      
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab4">
                <div class="content pdp-mod-review">
                  <div style="position: relative; width:100%; padding: 15px;">
                    <div style="clear:both;"></div>
                    <div id="map" style="width: 100%; height: 500px; position: relative; overflow: hidden;">
                      <iframe
                        style="width: 100%; height: 100%"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7838.431313490067!2d106.61716457653564!3d10.794788308238067!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752bf894db7137%3A0x8b48d821be179801!2zVMOibiBRdcO9LCBUw6JuIFBow7osIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1603428005724!5m2!1svi!2s"
                        frameborder="0" style="border:0;" allowfullscreen="true"
                        aria-hidden="false" tabindex="0"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--  PRODUCT OTHER -->
  

  <!--  PRODUCT RELATED -->
  
  </div>