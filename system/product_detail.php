<?php 
    include('config.php');
    $id = $_GET['id'];

    $sql ="SELECT products.*,brands.*,category.name as categoryName FROM products 
    LEFT JOIN brands
    ON products.brand = brands.id
    LEFT JOIN category
    ON products.category = category.id
    WHERE products.id=:id" ;
    $query= $dbh -> prepare($sql);
    $query-> bindParam(':id', $id, PDO::PARAM_STR);
    $query-> execute();
    $products_detail=$query->fetchAll(PDO::FETCH_OBJ);

?>