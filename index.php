<?php
  if(isset($_GET["a"])) {
    $data = $_GET["a"];
  } else {
    $data = "home";
  }
  if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
  
    include_once('./components/header.php');
    include_once('./components/search.php');
    include_once('./components/nav.php');
    include_once('./components/breadcrumb.php');
    include_once('./components/banner.php');
  switch ($data) {
    case 'home': 
      include_once 'home.php';
      break;
    case 'profile': 
      include_once 'profile.php';
      break;
    case 'order': 
        include_once 'order.php';
        break;
    case 'product': 
        include_once 'product.php';
        break;
    case 'logout':
    default:
      include_once 'home.php';
      break;
  }
  include_once('./components/footer.php');
?>


