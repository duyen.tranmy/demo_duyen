  <!--  KẾT NỐI CUNG CẦU -->
  <div class="connect-customer-wrap">
    <div class="container">
      <div class="row connect-customer">
        <div class="w-100 d-flex bg-green-light justify-content-between color-white">
          <div class="connect-customer-title text-uppercase font-weight-bold pl-3">
            KẾT NỐI CUNG CẦU
          </div>
          <div class="d-flex align-items-center pr-1 pr-sm-5 flex-wrap">
            <a href="#" class="pl-2 pl-sm-4">
              <img src="./img/bg-cart.png" width="130" alt="Cần mua">
            </a>
            <a href="#" class="pl-2 pl-sm-4">
              <img src="./img/bg-for-sale.png" width="130" alt="Cần bán">
            </a>
            <a href="#" class="pl-2 pl-sm-4">
              <img src="./img/bg-partner.png" width="130" alt="Tìm đối tác">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="product-line-wrap">
    <div class="container">
      <div class="row bg-white product-line justify-content-between">
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/borau.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/au.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/banhtet.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/ca.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/diathit.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
        <div class="product-line-item">
          <div class="d-flex">
            <div class="product-line-image pt-3 pb-3">
              <img src="./img/caibe.png" alt="Hình ảnh sản phẩm" width="100%">
            </div>
            <div class="product-line-info d-flex flex-column justify-content-center pl-4">
              <div class="product-line-title pb-2">Rau ngót sạch</div>
              <div class="product-line-author color-gray-blue">
                <img class="pb-1" src="./img/icon-user-gray.png" width="14px"> Thế Anh
                <span class="pl-1 pr-1">|</span>
                <img class="pb-1" src="./img/icon-clock.png" width="14px"> 3/9/2020
                <span class="pl-1 pr-1">|</span>
                <img src="./img/icon-menu-gray.png" width="14px"> Cần bán
              </div>
              <div class="product-line-price pt-2">8000đ/kg</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>