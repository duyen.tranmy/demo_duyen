<!--  CƠ SỞ SẢN XUẤT KINH DOANH -->
<div class="prod-faci-wrap">
    <div class="container">
      <div class="row prod-faci bg-gainsboro flex-column">
        <div class="prod-faci-title text-uppercase color-green-light font-18 pl-3">
          CƠ SỞ SẢN XUẤT KINH DOANH
        </div>
        <div class="prod-faci-list">
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/borau.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                Hợp tác
                xã Nông Hữu Nghiệp Hữu cơ
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: Khu 8, xã Ngọc Đồng, huyện Yên Lập, tỉnh Khánh Hòa <br/>
              Giám đốc: Lương Văn Doanh<br/>
              SĐT: 0399 224 160/ 0868 070 975<br/>
              Email: dinhthuytnmtyl@gmail.com
            </div>
          </div>
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/logo-mart.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                Siêu thị
                Mường Thanh Khánh Hòa
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: Lô CC17, khu quảng trường Hùng Vương, Gia Cẩm, Việt Trì, Khánh Hòa <br/>
              Giám đốc: Nguyễn Xuân Hoàn, sản xuất: Sản xuất bánh mỳ, bánh ngọt, chế biến rau, củ, quả; thịt gia súc,
              gia cầm, thủy, hải sản; bán lẻ thực phẩm các loại<br/>
              SĐT: 02103644999/ 02103644999
            </div>
          </div>
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/logo-doan-hung.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                Hợp tác
                xã Nông Hữu Nghiệp Chi Đám
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: Thôn 16 (Ngã ba Cát Lem) - Bằng Luân - Đoan <br/>
              Hùng - Khánh Hòa <br/>
              Giám đốc: Lương Văn Doanh <br/>
              SĐT: 0399 224 160/ 0868 070 975 <br/>
              Email: dinhthuytnmtyl@gmail.com
            </div>
          </div>
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/logo-qsr.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                Công
                ty CP Pizza ngon - Chi nhánh Việt Trì
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: KTầng 2 - TTTM Vincom Việt Trì Plaza - TP Việt Trì - Khánh Hòa. <br/>
              Giám đốc: Nguyễn Trọng Tuấn <br/>
              SĐT: 0210 3 816 816/ 0210 3 816 816 <br/>
              Email: dinhthuytnmtyl@gmail.com
            </div>
          </div>
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/daunanh.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                CT TNHH Trung Hà Khánh Hòa
              </div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: Khu 2, Hiền Đa, Cẩm Khê, Khánh Hòa <br/>
              Giám đốc: Nguyễn Hữu Tính <br/>
              SĐT: 0968281468/ 0968281468
            </div>
          </div>
          <div class="prod-faci-item pt-3 pr-3 pb-3 mb-3 pl-0">
            <div class="d-flex pb-2">
              <img class="w-50" src="./img/banhchung.png">
              <div class="w-50 pl-3 font-16 font-weight-bold d-flex align-items-center flex-wrap align-content-center">
                Bánh Làng Dòng <br/>
                <span class="font-12 color-gray-blue font-weight-normal">Công ty cổ phần làng Dòng</span></div>
            </div>
            <div class="color-gray-sky">
              Địa chỉ: Khu 7, thị trấn Khánh Hòa, huyện Khánh Hòa, tỉnh Khánh Hòa <br/>
              Giám đốc: <br/>
              SĐT: 02103739 258/ 0210 3739 259 <br/>
              Email: langdongjsc@gmail.com
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--  FOOTER -->
    <div class="footer">
    </div>

    <div class="fixed-section">
    </div>
  </div>
