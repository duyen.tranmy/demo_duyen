  <?php 
    include 'system/product.php';
  ?>
 <div class="product-list-wrap">
    <div class="container">
      <div class="row product-list">
        <?php 
          for($i = 0; $i<count($products1); $i++) {
            ?>
            <div class="product-item product-tab-item product-tab-1">
          <a href="<?php echo '?a=product&id='.$products1[$i]->id ?>" class="product-image">
          <img class="custom-img" src="<?php echo './img/'.$products1[$i]->image ?>">
          </a>
          <div class="product-info p-2 d-flex flex-column justify-content-between">
            <a href="<?php echo '?a=product&id='.$products1[$i]->id ?>" class="text-center">
              <div class="product-title pt-1 pb-1">
                <?php echo $products1[$i]->title?>
              </div>
              <div class="product-price color-green-light-2">
              <?php echo $products1[$i]->price?>  
              <span class="underline">đ</span></div>
            </a>
            <div class="product-bottom-info">
              <div class="product-place text-center">
              <?php echo $products1[$i]->content?>
              </div>
              <div class="d-flex justify-content-between">
                <div class="star-list">
                    <?php 
                    for($y= 0 ;$y < 5 ; $y++) {
                      if($y > $products1[$i]->rating - 1 ) {
                      ?>
                      <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>

                      <?php
                       } else {
                        ?>
                         <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>
                        <?php
                       }
                    }
                    ?>
                </div>
                <div class="cart-icon">
                  <img src="./img/icon-cart-defaul.png" alt="cart">
                </div>
              </div>
            </div>
          </div>
        </div>
            <?php
          }
        ?>

<?php 
          for($i = 0; $i<count($products2); $i++) {
            ?>
            <div class="product-item product-tab-item product-tab-2">
          <a href="<?php echo '?a=product&id='.$products2[$i]->id ?>" class="product-image">
            <img class="custom-img" src="<?php echo './img/'.$products2[$i]->image ?>">
          </a>
          <div class="product-info p-2 d-flex flex-column justify-content-between">
            <a href="<?php echo '?a=product&id='.$products2[$i]->id ?>" class="text-center">
              <div class="product-title pt-1 pb-1">
                <?php echo $products2[$i]->title?>
              </div>
              <div class="product-price color-green-light-2">
              <?php echo $products2[$i]->price?>  
              <span class="underline">đ</span></div>
            </a>
            <div class="product-bottom-info">
              <div class="product-place text-center">
              <?php echo $products2[$i]->content?>
              </div>
              <div class="d-flex justify-content-between">
                <div class="star-list">
                    <?php 
                    for($y= 0 ;$y < 5 ; $y++) {
                      if($y > $products2[$i]->rating - 1 ) {
                      ?>
                      <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>

                      <?php
                       } else {
                        ?>
                         <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>
                        <?php
                       }
                    }
                    ?>
                </div>
                <div class="cart-icon">
                  <img src="./img/icon-cart-defaul.png" alt="cart">
                </div>
              </div>
            </div>
          </div>
        </div>
            <?php
          }
        ?>
        <?php 
          for($i = 0; $i<count($products3); $i++) {
            ?>
            <div class="product-item product-tab-item product-tab-3">
          <a href="<?php echo '?a=product&id='.$products3[$i]->id ?>" class="product-image">
          <img class="custom-img" src="<?php echo './img/'.$products3[$i]->image ?>">
          </a>
          <div class="product-info p-2 d-flex flex-column justify-content-between">
            <a href="<?php echo '?a=product&id='.$products3[$i]->id ?>" class="text-center">
              <div class="product-title pt-1 pb-1">
                <?php echo $products3[$i]->title?>
              </div>
              <div class="product-price color-green-light-2">
              <?php echo $products3[$i]->price?>  
              <span class="underline">đ</span></div>
            </a>
            <div class="product-bottom-info">
              <div class="product-place text-center">
              <?php echo $products3[$i]->content?>
              </div>
              <div class="d-flex justify-content-between">
                <div class="star-list">
                    <?php 
                    for($y= 0 ;$y < 5 ; $y++) {
                      if($y > $products3[$i]->rating - 1 ) {
                      ?>
                      <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>

                      <?php
                       } else {
                        ?>
                         <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>
                        <?php
                       }
                    }
                    ?>
                </div>
                <div class="cart-icon">
                  <img src="./img/icon-cart-defaul.png" alt="cart">
                </div>
              </div>
            </div>
          </div>
        </div>
            <?php
          }
        ?>
        <?php 
          for($i = 0; $i<count($products4); $i++) {
            ?>
            <div class="product-item product-tab-item product-tab-4">
          <a href="<?php echo '?a=product&id='.$products4[$i]->id ?>" class="product-image">
          <img class="custom-img" src="<?php echo './img/'.$products4[$i]->image ?>">
          </a>
          <div class="product-info p-2 d-flex flex-column justify-content-between">
            <a href="<?php echo '?a=product&id='.$products4[$i]->id ?>" class="text-center">
              <div class="product-title pt-1 pb-1">
                <?php echo $products4[$i]->title?>
              </div>
              <div class="product-price color-green-light-2">
              <?php echo $products4[$i]->price?>  
              <span class="underline">đ</span></div>
            </a>
            <div class="product-bottom-info">
              <div class="product-place text-center">
              <?php echo $products4[$i]->content?>
              </div>
              <div class="d-flex justify-content-between">
                <div class="star-list">
                    <?php 
                    for($y= 0 ;$y < 5 ; $y++) {
                      if($y > $products4[$i]->rating - 1 ) {
                      ?>
                      <span class="star-item">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>

                      <?php
                       } else {
                        ?>
                         <span class="star-item active">
                      <img src="./img/icon-star-empty.png" width="16px" alt="star empty">
                      <img src="./img/icon-star.png" width="16px" alt="star">
                      </span>
                        <?php
                       }
                    }
                    ?>
                </div>
                <div class="cart-icon">
                  <img src="./img/icon-cart-defaul.png" alt="cart">
                </div>
              </div>
            </div>
          </div>
        </div>
            <?php
          }
        ?>
        

     
       

        
      </div>
    </div>
  </div>
