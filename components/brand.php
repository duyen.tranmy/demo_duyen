  <!--  THƯƠNG HIỆU ĐỒNG HÀNH -->
  <div class="useful-info-wrap">
    <div class="container">
      <div class="row useful-info bg-green-light justify-content-center color-white mb-2">
        <div class="useful-info-title text-uppercase font-weight-bold pl-3">
          THƯƠNG HIỆU ĐỒNG HÀNH
        </div>
      </div>
    </div>
  </div>

 <!-- DANH SÁCH THƯƠNG HIỆU -->
 <div class="trademark-wrap">
    <div class="container">
      <div class="row trademark bg-gainsboro pb-3">
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a1.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a2.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a3.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a4.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a5.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a6.png">
          </div>
        </div>

        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a7.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a8.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a9.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a10.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a11.png">
          </div>
        </div>
        <div class="trademark-item">
          <div class="trademark-wrap-image">
            <img class="custom-img" src="./img/a3.png">
          </div>
        </div>
      </div>
    </div>
  </div>