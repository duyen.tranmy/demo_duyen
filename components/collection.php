<div class="mid-collection-wrap">
    <div class="container">
      <div class="row mid-collection bg-green-light-2 justify-content-between color-white">
        <div class="mid-collection-title text-uppercase font-weight-bold pl-3">
          SẢN PHẨM
        </div>
        <div class="d-flex align-items-center mid-collection-list">
          <div class="js-mid-collection-item" data-class-tab="product-tab-1">Sản phẩm khuyến mãi</div>
          <div class="js-mid-collection-item" data-class-tab="product-tab-2">Sản phẩm mới</div>
          <div class="js-mid-collection-item" data-class-tab="product-tab-3">Sản phẩm được mua nhiều</div>
          <div class="js-mid-collection-item" data-class-tab="product-tab-4">Sản phẩm được xem nhiều</div>
        </div>
        <select class="d-none align-items-center mid-collection-list">
          <option class="js-mid-collection-item">Sản phẩm khuyến mãi</option>
          <option class="js-mid-collection-item">Sản phẩm mới</option>
          <option class="js-mid-collection-item">Sản phẩm được mua nhiều</option>
          <option class="js-mid-collection-item">Sản phẩm được xem nhiều</option>
        </select>
      </div>
    </div>
  </div>