<!--  THÔNG TIN HỮU ÍCH -->
<div class="useful-info-wrap">
    <div class="container">
      <div class="row useful-info bg-green-light justify-content-between color-white">
        <div class="useful-info-title text-uppercase font-weight-bold pl-3">
          THÔNG TIN HỮU ÍCH
        </div>
      </div>
    </div>
    <div class="container bg-white pt-3 pb-3">
      <div class="row">
        <div class="col-12 col-md-6 col-lg-4 mb-2">
          <div class="useful-title color-green font-18 pb-1">TIN TỨC</div>
          <div class="useful-content pt-3">
            <a href="#" class="useful-item mb-3">
              <img class="custom-img" src="./img/article-1.png">
              <div class="pl-3 pr-3 font-14">Mỗi xã một sản phẩm: Triển vọng từ mô hình nấm linh chi Ông Tiên</div>
            </a>
            <a href="#" class="useful-item mb-3">
              <img class="custom-img" src="./img/article-1.png">
              <div class="pl-3 pr-3 font-14">Mỗi xã một sản phẩm: Triển vọng từ mô hình nấm linh chi Ông Tiên</div>
            </a>
            <a href="#" class="useful-item mb-3">
              <img class="custom-img" src="./img/article-1.png">
              <div class="pl-3 pr-3 font-14">Mỗi xã một sản phẩm: Triển vọng từ mô hình nấm linh chi Ông Tiên</div>
            </a>
            <a href="#" class="useful-item mb-3">
              <img class="custom-img" src="./img/article-1.png">
              <div class="pl-3 pr-3 font-14">Mỗi xã một sản phẩm: Triển vọng từ mô hình nấm linh chi Ông Tiên</div>
            </a>
            <a href="#" class="useful-item mb-3">
              <img class="custom-img" src="./img/article-1.png">
              <div class="pl-3 pr-3 font-14">Mỗi xã một sản phẩm: Triển vọng từ mô hình nấm linh chi Ông Tiên</div>
            </a>
          </div>
        </div>

        <div class="col-12 col-md-6 col-lg-4 mb-2 d-flex flex-column">
          <div class="useful-title color-green font-18 pb-1">VIDEO</div>
          <div class="useful-content pt-3 flex-grow-1">
            <iframe width="100%" height="250px" src="https://www.youtube.com/embed/-OlpJrzDGzM" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
          </div>
        </div>

        <div class="col-12 col-md-6 col-lg-4 mb-2">
          <div class="useful-title color-green font-18 pb-1">VĂN BẢN</div>
          <div class="useful-content pt-3">
            <a href="#" class="useful-item useful-right-item mb-3">
              Về việc ban hành Quy định tiêu chí trình tự thủ tục, hồ sơ công nhận, công khai xã, phường, thị trấn an
              toàn thực phẩm trên địa bàn tỉnh
            </a>
            <a href="#" class="useful-item useful-right-item mb-3">
              Về việc ban hành Quy định tiêu chí trình tự thủ tục, hồ sơ công nhận, công khai xã, phường, thị trấn an
              toàn thực phẩm trên địa bàn tỉnh
            </a>
            <a href="#" class="useful-item useful-right-item mb-3">
              Thông tư quy định mức thu, chế độ thu, nộp, quản lý và sử dụng phí trong công tác an toàn vệ sinh thực
              phẩm
            </a>
            <a href="#" class="useful-item useful-right-item mb-3">
              Về việc, bổ sung thành viên Tổ thẩm định xã, phường, thị trấn an toàn thực phẩm
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>