<div class="top-filter-wrap bg-white">
    <div class="container">
      <div class="row top-filter justify-content-between align-items-center">
        <img class="top-logo" src="./img/logo.png" width="170px">
        <div class="top-filter-and-cart d-flex flex-grow-1 justify-content-end">
          <div class="top-input-filter">
            <input class="input-filter w-100 input-filter-1" placeholder="Tìm kiếm thông tin trên trang">
          </div>
          <div class="top-input-filter">
            <input class="input-filter w-100 input-filter-2" placeholder="Nhập vị trí của bạn">
            <div class="input-filter-2-dropdown">
              <div class="d-flex pt-2 align-items-center input-filter-2-dropdown-item" data-selected="HCM, Việt Nam">
                <img class="pb-1 mr-2" src="./img/icon-googlemap-gray.png" alt="icon google map" width="14px">
                HCM <span class="ml-2">Việt Nam</span>
              </div>
              <div class="d-flex pt-2 align-items-center input-filter-2-dropdown-item"
                   data-selected="Đà Nẵng, Việt Nam">
                <img class="pb-1 mr-2" src="./img/icon-googlemap-gray.png" alt="icon google map" width="14px">
                Đà Nẵng <span class="ml-2">Việt Nam</span>
              </div>
              <div class="d-flex pt-2 align-items-center input-filter-2-dropdown-item" data-selected="Hà Nội, Việt Nam">
                <img class="pb-1 mr-2" src="./img/icon-googlemap-gray.png" alt="icon google map" width="14px">
                Hà Nội <span class="ml-2">Việt Nam</span>
              </div>
            </div>
          </div>
          <div class="bg-orange btn-icon-search" onclick="search(this)">
            <img src="./img/icon-search.png" width="18px">
          </div>
        </div>
        <a href="#" class="btn-icon-cart">
          <img src="./img/icon-cart-orange.png" width="30px">
          <span class="num-cart-item badge">10</span>
          <div class="cart-web-hover">
            <div class="cart-sp">
              <div>
                <div class="cart-content-title v-center">Sản phẩm mới thêm</div>
                <div class="product-items-cart">
                  <div class="cart-web-hover-item">
                    <div class="cart-web-item-left" style="background-image: url('./img/product-detail-1.jpeg');"></div>
                    <div class="cart-web-item-right">
                      <div class="v-center">
                        <div class="cart-sp-name">Dưa vàng Hoàng Kim Hậu</div>
                        <div class="_2BMmIF"></div>
                        <div class="cart-sp-price v-center">
                          <div>80.000&nbsp;₫</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="cart-web-item-footer">
                  <div class="navbar__spacer"></div>
                  <button
                    class="btn btn-solid-primary btn--s btn--inline d-flex align-items-center justify-content-center">
                    Xem Giỏ hàng
                  </button>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>