<!doctype html>
<html lang="vi">
<head>
  <!-- global -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Trang Chủ - Kết nối cung cầu tỉnh Khánh Hòa</title>

  <link rel="stylesheet" href="./css/reset.css"/>
  <link rel="stylesheet" href="./css/bootstrap.min.css"/>
  <link rel="stylesheet" href="./css/owl.carousel.min.css"/>
  <link rel="stylesheet" href="./css/owl.theme.default.css"/>
  <!--  my css -->
  <link rel="stylesheet" href="./css/style.css"/>
  <link rel="stylesheet" href="./css/responsive.css"/>
</head>
<body>
<div class="page-wrap bg-white">

  <!--  TOP HEAD -->
  <div class="top-head-wrap bg-green-light color-white">
    <div class="container">
      <div class="row top-head justify-content-end color-white">
        <div class="menu-mobile">
          <div class="position-relative">
            <img class="icon-menu-mobile" src="./img/icon-menu-white.png" width="30px">
            <div class="menu-mobile-list">
              <div class="menu-mobile-item">
                <a href="#" class="menu-mobile-lvl-1 justify-content-start" data-toggle="collapse">
                  Trang chủ
                </a>
              </div>
              <div class="menu-mobile-item">
                <a href="#" class="menu-mobile-lvl-1 justify-content-start" data-toggle="collapse">
                  Kết nối cung cầu
                </a>
              </div>
              <div class="menu-mobile-item">
                <a href="#menu1" class="menu-mobile-lvl-1" data-toggle="collapse">
                  Nguồn cung cấp
                  <img src="./img/icon-arrow-right-green.png" width="8px"/>
                </a>
                <div id="menu1" class="collapse">
                  <a class="menu-mobile-lvl-2" href="#menu11" data-toggle="collapse">
                    Menu 1 1
                    <img src="./img/icon-plus-green.png" width="8px">
                  </a>
                  <div id="menu11" class="collapse menu-mobile-wrap-lvl-3">
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 1 1</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 1 2</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 1 3</a>
                    </div>
                  </div>
                  <a class="menu-mobile-lvl-2" href="#menu12" data-toggle="collapse">
                    Menu 1 2
                    <img src="./img/icon-plus-green.png" width="8px">
                  </a>
                  <div id="menu12" class="collapse menu-mobile-wrap-lvl-3">
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 2 1</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 2 2</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 1 2 3</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="menu-mobile-item">
                <a href="#menu2" class="menu-mobile-lvl-1" data-toggle="collapse">
                  Thông tin hữu ích
                  <img src="./img/icon-arrow-right-green.png" width="8px"/>
                </a>
                <div id="menu2" class="collapse">
                  <a class="menu-mobile-lvl-2" href="#menu21" data-toggle="collapse">
                    Menu 2 1
                    <img src="./img/icon-plus-green.png" width="8px">
                  </a>
                  <div id="menu21" class="collapse menu-mobile-wrap-lvl-3">
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 1 1</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 1 2</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 1 3</a>
                    </div>
                  </div>
                  <a class="menu-mobile-lvl-2" href="#menu22" data-toggle="collapse">
                    Menu 2 2
                    <img src="./img/icon-plus-green.png" width="8px">
                  </a>
                  <div id="menu22" class="collapse menu-mobile-wrap-lvl-3">
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 2 1</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 2 2</a>
                    </div>
                    <div class="menu-mobile-child-item">
                      <a class="menu-mobile-lvl-3" href="#">Menu 2 2 3</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="curtain"></div>
          </div>
        </div>
        <?php 
          include 'sign.php'
        ?>
      </div>
    </div>
  </div>
