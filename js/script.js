var customRootColor = {
  'green-light': '#4fa305', //bg
  'green-light-2': '#5bb504', //bg
  'green-light-3': '#40a300', //price color
  'green': '#00a651', //bg
  'blue-dark': '#4267b2',
  'gray': '#a6a6c4', //bg
  'gray-blue': '#a6a6c4', //bg
  'gray-sky': '#9eacb5', //bg
  'gray-dark': '#333333', //bg
  'gray-light': '#dedfe0', //bg
  'gainsboro': '#f6f6f6', //bg
  'gainsboro-2': '#f1f2f2', //bg
  'gainsboro-light': '#fafafa', //bg
  'pink': '#f4a2a6',//bg
  'orange-light': '#f37021',//bg
  'orange': '#fb7c2f', //icon search
  'orange-heavy': '#f37021', //icon search
  'orange-dark': '#ff5722', //icon search
  'white': '#ffffff',
}

//init css
var element = document.createElement("style");
element.id = "initStyle"; // so you can get and alter/replace/remove later

Object.keys(customRootColor).forEach(key => {
  document.documentElement.style.setProperty(`--color-${key}`, `${customRootColor[key]}`);
  element.innerHTML += `.color-${key} {color: ${customRootColor[key]};}`
  element.innerHTML += `.bg-${key} {background-color: ${customRootColor[key]};}`
  element.innerHTML += `.border-${key} {border-color: ${customRootColor[key]};}`
})

document.getElementsByTagName("HEAD")[0].appendChild(element);
//end init css

$('.icon-menu-mobile').click(() => {
  $('.nav-mobile').addClass('on')
})

$('.btn-close-mobile-menu').click(() => {
  $('.nav-mobile').removeClass('on')
})

//
$('[data-href]').on('click', event => {
  let href = $(event.target).closest('[data-href]').attr('data-href')
  if (!href.length) return
  location.href = href
})

function setActiveTab(event) {
  $('.js-mid-collection-item').removeClass('active')
  $(event.target).addClass('active')
  $('.product-tab-item').hide()
  $('.product-tab-item').each((idx, item) => {
    if ($(item).hasClass($(event.target).attr('data-class-tab'))) {
      $(item).show()
    }
  })
}

$('.js-mid-collection-item').click(event => {
  setActiveTab(event)
})

$('select.mid-collection-list').change(e => {
  let idx = e.target.selectedIndex
  let $option = $('.js-mid-collection-item').eq(idx)
  $option.trigger('click')
})

$('.icon-menu-mobile').click(event => {
  $('.menu-mobile-list').toggleClass('active')
})


$('.input-filter-2-dropdown').hide()
$('.input-filter-2').focus(e => {
  $('.input-filter-2-dropdown').addClass('show')
})

$('.input-filter-2').blur(e => {
  setTimeout(() => {
    $('.input-filter-2-dropdown').removeClass('show')
  }, 500)
})

$('.input-filter-2-dropdown-item').click(e => {
  let val = $(e.target).closest('.input-filter-2-dropdown-item').attr('data-selected')
  $('.input-filter-2').val(val)
  console.log(val);
})


//
$('.top-banner-owl-carousel').owlCarousel({
  loop: true,
  margin: 0,
  dots: false,
  autoplay: true,
  responsive: {
    0: {
      items: 1
    }
  }
})

//zoom image: product detail

let $productImages = $('#product-image-featured').length && $('#product-image-featured').ezPlus({
  gallery: 'product-image-featured',
  galleryActiveClass: 'active',
  zoomWindowFadeIn: 300,
  zoomWindowFadeOut: 300,
  lensFadeOut: 300
});


//
$('.product-image-item img').click(e => {
  $('.product-image-item').removeClass('active')
  $(e.target).closest('.product-image-item').addClass('active')
  let largeImage = $(e.target).attr('data-image')
  let smallImage = $(e.target).attr('data-image')
  let ez = $('#product-image-featured').data('ezPlus');
  ez.swaptheimage(smallImage, largeImage);
})

function increase() {
  var result = document.getElementById('qty');
  var qty = result.value;
  if (!isNaN(qty)) {
    result.value++;
  }
  return false;
}

function reduced() {
  var result = document.getElementById('qty');
  var qty = result.value;
  if (!isNaN(qty) && qty > 1)
    result.value--;
  return false;
}

function showModalRate() {
  $('#modal-rate').modal();
}

function poupModalSignup(event) {
  console.log(1)
  $('.modal-signin').addClass('popup')
  $('.modal-message.signup').addClass('active')
  
  showForm('register')
}

function poupModalSignin(event) {
  console.log(1)
  $('.modal-signin').addClass('popup')
  $('.modal-message.signin').addClass('active')
  showForm('login')
}

function showForm(formName, event) {
  console.log(event)
  if (formName == 'register') {
    $('form.register-form').show()
    $('form.login-form').hide()
  }
  if (formName == 'login') {
    $('form.login-form').show()
    $('form.register-form').hide()
  }
  if (event) {
    $('.modal-message').removeClass('active')
    $(event.target).addClass('active')
  }
}

$('.modal-message.signin').click(function (event) {
  console.log(1)
  showForm('login', event)
});

$('.modal-message.signup').click(function (event) {
  console.log(1)
  showForm('register', event)
});

$('.close-popup').click(event => {
  console.log(1)
  $(event.target).closest('.modal-custom').removeClass('popup')
  $('.modal-message').removeClass('active')
})
